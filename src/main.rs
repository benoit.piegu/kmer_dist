use ahash::AHasher;
use anyhow::Context;
use funty::{Integral, Unsigned};
use itertools::Itertools;
use kmer_dist::out::Out;
use memchr::memchr;
use ndarray::{Array, ArrayBase, Dim, OwnedRepr};
use ndarray_npy::WriteNpyExt;
use ndarray_stats::QuantileExt;
use needletail::parse_fastx_reader;
use num_traits::identities;
use std::{
    collections::HashMap,
    error::Error,
    fmt::Display,
    fs::File,
    hash::{BuildHasher, BuildHasherDefault, Hasher},
    io::{BufReader, Read, Write},
    path::PathBuf,
};

use clap::{arg, command, crate_description, value_parser};

const INPUT_DEFAULT_BUFSIZE: usize = 1024 * 8;

// from crate num, num::traits::cast::AsPrimitive
pub trait AsPrimitive<T>: 'static + Copy
where
    T: 'static + Copy,
{
    /// Convert a value to another, using the `as` operator.
    fn as_(self) -> T;
}

/// get id of a sequence from the header line (fasta/fastq formats)
///
/// # Aguments
///
/// * seq_header - the seq header as an `&[u8]`
///
/// # Examples
///
/// ```
/// use seqcollapse::get_seq_id;
///
/// let seq_header = b"AB-12 1:N:0:ATTCCT";
/// let seq_id = get_seq_id(seq_header);
/// assert_eq!(seq_id, &seq_header[0..5]);
/// ```
///
pub fn get_seq_id(seq_header: &[u8]) -> &[u8] {
    let pos_end_of_id = memchr(b' ', seq_header).unwrap_or(seq_header.len());
    &seq_header[0..pos_end_of_id]
}

/// from a vector of fasta files, read sequences
/// and return a tuble containung
///
/// * an HashMap containing for each kmer dectected, the total count
/// * the total number of sequences
/// * the number of sequences which can produce kmer (seq.len() > kmer_size)
/// * the length of the longest sequence
///
///
///  # Arguments
///
/// * `fastx_files`: a slice of filename
/// * `kmer_size`: length of kmer
fn kmer_count<P, U, H>(
    fastx_files: &[P],
    kmer_size: u8,
) -> Result<
    (
        HashMap<Vec<u8>, U, BuildHasherDefault<H>>,
        usize,
        usize,
        usize,
    ),
    Box<dyn Error>,
>
where
    PathBuf: From<P>,
    P: Clone + Display, //AsRef<Path>>
    U: Unsigned,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    let mut nseq_tot: usize = 0;
    let mut nseq_ok = 0;
    let mut max_lseq: usize = 0;
    let mut hash_kmer: HashMap<Vec<u8>, U, BuildHasherDefault<H>> = HashMap::default();
    for fastx_path in fastx_files.iter() {
        let file = File::open::<PathBuf>(<P as Into<PathBuf>>::into(fastx_path.clone()))
            .with_context(|| format!("couldn't open <{}>", &fastx_path))?;
        let buff =
            Box::new(BufReader::with_capacity(INPUT_DEFAULT_BUFSIZE, file)) as Box<dyn Read + Send>;
        let mut seqreader =
            parse_fastx_reader(buff).with_context(|| format!("can't parse <{}>", fastx_path))?;
        let mut nseq: usize = 0;
        while let Some(seqrec) = seqreader.next() {
            let seqrec = seqrec.with_context(|| "Can't parse fastx stream")?;
            nseq += 1;
            let seq = seqrec.seq();
            max_lseq = max_lseq.max(seq.len());
            if seq.len() >= kmer_size as usize {
                nseq_ok += 1;
                seq.windows(kmer_size as usize).for_each(|k| {
                    if let Some(new_count) = hash_kmer.get_mut(k) {
                        *new_count += U::ONE;
                    } else {
                        hash_kmer.insert(k.to_vec(), U::ONE);
                    }
                });
            }
        }
        nseq_tot += nseq;
    }
    Ok((hash_kmer, nseq_tot, nseq_ok, max_lseq))
}

fn hcount_metrics<K, U, H>(hash_kmer: &HashMap<K, U, BuildHasherDefault<H>>) -> (U, U, u64)
where
    U: Unsigned,
    H: Hasher,
{
    let (min, max, nsingletons) = hash_kmer
        .iter()
        .fold((U::MAX, U::ZERO, 0u64), |e, (_k, c)| {
            (
                e.0.min(*c),
                e.1.max(*c),
                if *c == U::ONE { e.2 + 1 } else { e.2 },
            )
        });
    (min, max, nsingletons)
}

fn create_matrix<U, V, H>(
    fastx_files: &Vec<String>,
    kmer_size: u8,
    nseq_ok: usize,
    hash_kmer: &HashMap<Vec<u8>, U, BuildHasherDefault<H>>,
    mut out_seqid: Box<dyn Write>,
) -> Result<ArrayBase<OwnedRepr<V>, Dim<[usize; 2]>>, Box<dyn Error>>
where
    U: Unsigned,
    V: Integral + identities::Zero,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    let mut mat: ArrayBase<OwnedRepr<V>, Dim<[usize; 2]>> =
        Array::<V, _>::zeros((nseq_ok, hash_kmer.len()));
    let mut iseq: usize = 0;
    for fastx_path in fastx_files.iter() {
        let file =
            File::open(fastx_path).with_context(|| format!("couldn't open <{}>", &fastx_path))?;
        let buff =
            Box::new(BufReader::with_capacity(INPUT_DEFAULT_BUFSIZE, file)) as Box<dyn Read + Send>;

        let mut seqreader =
            parse_fastx_reader(buff).with_context(|| format!("can't parse <{}>", fastx_path))?;
        while let Some(seqrec) = seqreader.next() {
            let seqrec = seqrec.with_context(|| "Can't parse fastx stream")?;
            let seq = seqrec.seq();
            if seq.len() >= kmer_size as usize {
                writeln!(out_seqid, "{}\t{}", iseq, unsafe {
                    std::str::from_utf8_unchecked(get_seq_id(&seqrec.id()))
                })?;
                seq.windows(kmer_size as usize).for_each(|k| {
                    if let Some(ikmer) = hash_kmer.get(k) {
                        mat[[iseq, ikmer.as_usize()]] += V::ONE;
                    } else {
                        // err
                        eprintln!("# Err: kmer {} doesn't exist", unsafe {
                            std::str::from_utf8_unchecked(k)
                        });
                        todo!()
                    }
                });
                iseq += 1;
            }
        }
    }
    Ok(mat)
}

fn main() -> core::result::Result<(), Box<dyn Error>> {
    let app = command!()
        .about(crate_description!())
        .arg_required_else_help(true)
        .arg(
            arg!(-d --debug ... "Sets the level of debugging")
                .required(false)
                .action(clap::ArgAction::Count),
        )
        .arg(
            arg!(-k --kmer [KMER] "set the length of kmer")
                .help("Size of kmer")
                .default_value("7")
                .value_parser(value_parser!(u8).range(1..255)),
        )
        .arg(
            arg!(-i --info [INFO] "Don't return the matrix count of kmer. Just estimate size of matrix and print some infos about kmer")
                .required(false)
                .action(clap::ArgAction::SetTrue),
        )
        .arg(
            arg!(-o --out <OUT_FILE> "Out file for kmer count matrix")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .required(false),
        )
        .arg(
            arg!(-k --kmer_out <OUT_FILE> "Out file for kmer index")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .required(false),
        )
        .arg(
            arg!(-s --seqid_out <OUT_FILE> "Out file for sequence id. If no defined sequence id are printed to STDERR")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .required(false),
        )
        .arg(
            arg!(<FASTX> ... "Sets FASTX files to read")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .num_args(clap::builder::ValueRange::new(1..)),
        );

    let arguments = &app.get_matches();
    let kmer_size = *arguments.get_one::<u8>("kmer").unwrap();
    let only_info = arguments.get_flag("info");
    let fseqs: Vec<String> = arguments
        .get_many::<String>("FASTX")
        .unwrap()
        .map(|s| s.clone())
        .collect();

    let (mut h, nseq_tot, nseq_ok, max_lseq) =
        kmer_count::<_, u32, AHasher>(&fseqs[..], kmer_size)?;
    let n_kmer = h.len();
    if nseq_ok == 0 {
        eprintln!("There is no sequence longer than kmer_size ({})", kmer_size);
        return Ok(());
    }
    let (min, max, nsingletons) = hcount_metrics(&h);
    eprintln!(
        "# n sequences tot = {} -- n sequences with length >= {} = {} -- length max of seq: {}",
        nseq_tot, kmer_size, nseq_ok, max_lseq
    );
    eprintln!(
        "# kmer_size: {} => {} distincts kmers for {} kmers -- theoretical number of kmers: {:.3e}",
        kmer_size,
        h.len(),
        n_kmer,
        20.0f32.powf(kmer_size as f32)
    );
    eprintln!(
        "# count range: {}:{} -- {} ({:.2}%) singletons",
        min,
        max,
        nsingletons,
        nsingletons as f32 / h.len() as f32 * 100.0
    );

    // determine the minimum type to store counts of kmer
    let (s_type, size_in_bytes) = match (max_lseq as u64 - kmer_size as u64) as u64 {
        m if m <= u8::MAX as u64 => ("u8", 1u8),
        m if m <= u16::MAX as u64 => ("u16", 2),
        m if m <= u32::MAX as u64 => ("u32", 4),
        _ => ("u64", 8),
    };
    // matrix size
    eprintln!(
        "# Matrix size: {} sequences x {} kmers of <{}> ({} bytes) => {:.2}G ",
        nseq_ok,
        h.len(),
        s_type,
        size_in_bytes,
        nseq_tot as f32 * h.len() as f32 * size_in_bytes as f32 / 1024f32.powf(3.0),
    );
    if !only_info {
        // transform hashmap of kmer count
        // into hashmap of kmer index
        for (i, c) in h.values_mut().enumerate() {
            *c = i as u32
        }
        if let Some(f) = arguments.get_one::<String>("kmer_out") {
            let mut out = Out::create_file(f, None)?;
            h.iter()
                .sorted_unstable_by(|a, b| Ord::cmp(&a.1, &b.1))
                .try_for_each(|(kmer, ikmer)| {
                    writeln!(
                        out.out,
                        "{}\t{}",
                        unsafe { std::str::from_utf8_unchecked(&kmer[..]) },
                        ikmer
                    )
                })?;
        }
        let out_seqid = Out::create_file_or_stderr(arguments.get_one::<String>("seqid_out"), None)?;
        let mat: ArrayBase<OwnedRepr<u16>, Dim<[usize; 2]>> =
            create_matrix(&fseqs, kmer_size, nseq_ok, &h, out_seqid.out)?;
        let out = Out::create_file_or_stdout(arguments.get_one::<String>("out"), None)?;
        eprintln!(
            "# mat: shape={:?} max={:?} sum={}",
            &mat.shape(),
            &mat.max(),
            // can't use mat.sum() beacause the return type is the same than elements of mat
            &mat.iter().fold(0u64, |sum, &c| sum + c as u64)
        );
        mat.write_npy(out.out)?;
    }
    Ok(())
}
